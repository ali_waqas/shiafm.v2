import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the MusicQueue provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/

@Injectable()

export class MusicQueue {
	public StorageName: string = 'ShiaFM::MusicQueue';
	public storage: any = window.localStorage;
	public Queue: Array<object> = [];

	constructor(public http: Http) {
		this.openQueue();
	}

	createQueue(){
		if(!this.storage.getItem(this.StorageName)){
			this.storage.setItem(this.StorageName,{});
			console.log(this.StorageName+' created');
		}else{
			console.log(this.StorageName+' already available');
		}
	}

	openQueue(){
		this.Queue = JSON.parse(this.storage.getItem(this.StorageName));
	}

	push(song: object){
		this.Queue.push(song);
		this.save();
	}

	removeFromQueueByID(ID: number){
		let removed = false;
		this.Queue.forEach(function(Song, index, object) {
		  if (Song['id'] === ID) {
		  	object.splice(index, 1);
		  	removed = true;
		  }
		});
		if(removed) this.save();
	}

	save(){
		this.storage.setItem(this.StorageName,JSON.stringify(this.Queue));
	}

	clear(){
		this.storage.setItem(this.StorageName,JSON.stringify([]));
	}

	insertSampleData(){
		this.push({    "id": 300002470,
					    "parent": 200000364,
					    "title": "بثار الغريب",
					    "isDir": false,
					    "isVideo": false,
					    "type": "music",
					    "albumId": 200000364,
					    "album": "الصبر على الأشجان",
					    "artistId": 100000084,
					    "artist": "الشيخ زكي البحراني",
					    "coverArt": 200000364,
					    "duration": 179,
					    "bitRate": 48,
					    "genre": "Latim-Ar",
					    "size": 1095400,
					    "suffix": "mp3",
					    "contentType": "audio/mpeg",
					    "path": "الشيخ زكي البحراني/الصبر على الأشجان/ الغريب.mp3"});

		this.push({      "id": 300013647,
					    "parent": 200000072,
					    "title": "كربلا عدنا من جديد",
					    "isDir": false,
					    "isVideo": false,
					    "type": "music",
					    "albumId": 200000072,
					    "album": "NoAlbum",
					    "artistId": 100000272,
					    "artist": "ناجي حيدر",
					    "coverArt": 200000072,
					    "duration": 1219,
					    "bitRate": 32,
					    "genre": "Latim-Ar",
					    "size": 4883863,
					    "suffix": "mp3",
					    "contentType": "audio/mpeg",
					    "path": "ناجي حيدر/NoAlbum/ عدنا من جديد.mp3"});
	}

}
