import { Component, ViewChild } from '@angular/core';
import { Nav, Platform,Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { Playlists } from '../pages/playlists/playlists';


@Component({
  templateUrl: 'app.html',
  providers:[]
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  Profile: any;
  _ProfileService : any;
  _LoginService: any;
  user: any;
  pages: Array<{title: string, component: any, icon: any, badge: any}>;
  ProfileReady:boolean = false;

  constructor(
                public platform: Platform, 
                public statusBar: StatusBar, 
                public splashScreen: SplashScreen, 
                events: Events
              ) {
    this.initializeApp();
    this.pages = [
                    { title: 'Listen Now', component: HomePage, icon:'musical-notes', badge: false },
                    { title: 'Recents', component: HomePage, icon:'refresh',badge: false},
                    { title: 'Music Library', component: HomePage, icon:'albums', badge: false},
                    { title: 'Settings', component: HomePage, icon:'settings', badge: false },
                    { title: 'Sign Out', component: HomePage, icon:'log-out', badge: false },
                ];
  }
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
