import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { System } from './system';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

/*
  Generated class for the Ampache provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Ampache {

	public Server = "http://shiafm.com"; // 
	public ServerURI = 'rest';
	public Username = 'admin';
	public Password = 'admin!2';
	public Version = '1.1.0';
	public AppName = 'myapp';
	public ReturnType = "json";
	public AmpacheResponse: any;
	public Response: Observable<string>;

	constructor(
					public http: Http,
					public System: System
				) {
		console.log('Hello Ampache Provider');
	}

	createURI = function(View,Xtra=null){
	    var xtra = (xtra!=null)?'&void=true':xtra;
	    return this.Server+'/'+this.ServerURI+'/'+View+'.view?u='+this.Username+'&p='+this.Password+'&v='+this.Version+'&c='+this.AppName+'&f='+this.ReturnType+Xtra;
	}

	request = function(url){
		return this.http.get(url).map(res => res.json());
	}

	callAPIMethod(Method:string, extra: string = null){
		this.ReturnType = 'json';
		let url = this.createURI(Method,extra);
		return this.http.get(url).map(res => res.json());
	}

	getAmpacheObject(AmpacheResponse,key){
		if(AmpacheResponse){
			AmpacheResponse = AmpacheResponse['subsonic-response'];
			if(AmpacheResponse.hasOwnProperty(key)){
				return AmpacheResponse[key];
			}else{
				return null;
			}
		}
	}
}
