import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ToastController } from 'ionic-angular';
import 'rxjs/add/operator/map';


/*
  Generated class for the System provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class System {
  public ApiEndPoint: string = 'http://iplus.dev:8888/';
  public mode = 'Pro';
  public http:any;
  constructor(
                public _http: Http,
                public ToastController: ToastController
             ){
    this.http = _http;
  }

  post(url,params){
  	params['api_request'] = true;
  	params['is_ajax_request'] = true;
  	var headers = new Headers();
  	headers.append('Content-Type', 'application/x-www-form-urlencoded');
  	return this.http.post(this.getAPIEndPoint()+url, params, {headers: headers});
  }

  get(url: string ){
  	return this.http.get(this.getAPIEndPoint()+url);
  }

  getAPIEndPoint(){
  	return (this.mode=='Dev')?'http://iplus.dev:8888/':'http://iplus.com.cy/';
  }

  showToast(Message: string){
    let toast = this.ToastController.create({
      message: Message,
      duration: 3000
    });
    toast.present();
  }
}
