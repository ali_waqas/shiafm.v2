import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { System } from '../../providers/system';
import { Ampache } from '../../providers/ampache';
import { Artist } from '../artist/artist';
import { AlertController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular';

/**
 * Generated class for the Playlists page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'geners',
  templateUrl: 'geners.html',
})

export class Geners {
	public Geners: any;
	public Loaded: boolean = false;
	public Response: any;
	public SelectedID: any;
	public EditMode: boolean = false;

	constructor(
					public navCtrl: NavController, 
					public navParams: NavParams,
					public SystemService: System,
					public Ampache: Ampache,
					public AlertController: AlertController,
					public ActionSheetController: ActionSheetController,
				 ) {
		if(!this.Loaded){
			this.listGeners();
		}
	}

	listGeners(){
		this.Ampache.callAPIMethod('getGenres','')
		.subscribe(
	              data => {
	              	let _data = this.Ampache.getAmpacheObject(data,'genres');
	              	this.Geners = _data.genre;
	              },
	              error =>{
	              	console.log(error);
	              }
				 );	
	}
}
