import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { System } from '../../providers/system';
import { Ampache } from '../../providers/ampache';
import { Artist } from '../artist/artist';
import { AlertController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

/**
 * Generated class for the Playlists page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'artists',
  templateUrl: 'artists.html',
})

export class Artists {
	public Playlists: any;
	public Artists: any;
	public Artist: any;
	public Loaded: boolean = false;
	public Response: any;
	public SelectedID: any;
	public EditMode: boolean = false;
	public Loader: any;
	constructor(
					public navCtrl: NavController, 
					public navParams: NavParams,
					public SystemService: System,
					public Ampache: Ampache,
					public AlertController: AlertController,
					public ActionSheetController: ActionSheetController,
					public LoadingController: LoadingController
				 ) {

		if(!this.Loaded){
			this.listArtists();
		}
	}

	listArtists(){
		this.Loader = this.LoadingController.create({
		  content: "Please wait...",
		  duration: 3000
		});
		this.Loader.present();
		this.Ampache.callAPIMethod('getArtists','')
		.subscribe(
	              data => {
	              	let _data = this.Ampache.getAmpacheObject(data,'artists');
	              	this.Artists = _data.index.artist;
	              	//this.Loader.dismiss();
	              },
	              error =>{
	              	console.log(error);
	              }
				 );	
	}

	openArtist(ArtistID: number){
		this.Loader = this.LoadingController.create({
		  content: "Please wait...",
		  duration: 3000
		});
		this.Loader.present();
		this.Ampache.callAPIMethod('getArtist','&id='+ArtistID)
		.subscribe(
                    data => {
                    	this.Artist = this.Ampache.getAmpacheObject(data,'artist');
                    	this.navCtrl.push(Artist,{Artist:this.Artist});
                    	this.Loader.dismiss();
                    },
                    error =>{
                    	console.log(error);
                    }
				 );	
	}



}
