import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { System } from '../../providers/system';
import { Ampache } from '../../providers/ampache';
import { Album } from '../album/album';
import { AlertController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular';

/**
 * Generated class for the Playlists page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'playlists',
  templateUrl: 'playlists.html',
})
export class Playlists {
	public Playlists: any;
	public Albums: any;
	public Album: any;
	public Loaded: boolean = false;
	public Response: any;
	public SelectedID: any;
	public EditMode: boolean = false;

	constructor(
					public navCtrl: NavController, 
					public navParams: NavParams,
					public SystemService: System,
					public Ampache: Ampache,
					public AlertController: AlertController,
					public ActionSheetController: ActionSheetController,
				 ) {
		if(!this.Loaded){
			this.getPlaylists();
		}
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad Playlists');
	}

	getPlaylists(){
		this.Ampache.callAPIMethod('getPlaylists','')
		.subscribe(
	              data => {
	              	let dataa = this.Ampache.getAmpacheObject(data,'playlists');
	              	this.Playlists = dataa.playlist;
	              	console.log(this.Playlists);
	              },
	              error =>{
	              	console.log(error);
	              }
				 );	
	}

	openAlbum(AlbumID: number){
		this.Ampache.callAPIMethod('getAlbum','&id='+AlbumID)
		.subscribe(
                    data => {
                    	this.Album = this.Ampache.getAmpacheObject(data,'album');
                    	this.navCtrl.push(Album,{Album:this.Album});
                    },
                    error =>{
                    	console.log(error);
                    }
				 );	
	}

	createPlaylist(Name: string){
		let string = (this.EditMode)? '&name='+Name+'&playlistId='+this.SelectedID : '&name='+Name;
		let url = (this.EditMode)? 'updatePlaylist':'createPlaylist';
		this.Ampache.callAPIMethod(url,string)
		.subscribe(
	              data => {
	              	this.Response = this.Ampache.getAmpacheObject(data,'status');
	              	if(this.Response=='ok'){
	              		this.EditMode = false;
	              		this.SystemService.showToast('Playlist created');
	              		this.getPlaylists();
	              	}
	              },
	              error =>{
	              	console.log(error);
	              }
				 );	
	}

	showPromptForNewPlaylist(Name: string = null) {
	    let prompt = this.AlertController.create({
	      title: 'Playlist',
	      message: "Playlist Name: ",
	      inputs: [{
		      			name: 'title',
		          		placeholder: 'Title',
		          		value: Name
	        	    },
	      		  ],
	      buttons: [
	        {
	          text: 'Cancel',
	          handler: data => {
	            console.log('Cancel clicked');
	          }
	        },
	        {
	          text: 'Save',
	          handler: data => {
	          	this.createPlaylist(data.title);
	          }
	        }
	      ]
	    });
	    prompt.present();
	}

	showPromptForEditPlaylist(PlaylistID: number) {
		for(let i=0;i<this.Playlists.length;i++){
			if(this.Playlists[i].id==PlaylistID){
				let Playlist = this.Playlists[i];
				this.EditMode = true;
				this.showPromptForNewPlaylist(Playlist.name);
			}
		}
	}

	deletePlaylist(playlistID: number){
		this.Ampache.callAPIMethod('deletePlaylist','&id='+playlistID)
		.subscribe(
	              data => {
	              	this.Response = this.Ampache.getAmpacheObject(data,'status');
	              	if(this.Response=='ok'){
	              		this.SystemService.showToast('Playlist removed');
	              		this.getPlaylists();
	              	}
	              },
	              error =>{
	              	console.log(error);
	              }
		);	
	}

	presentActionSheet(playlistID: number) {
		this.SelectedID = playlistID;
	    let actionSheet = this.ActionSheetController.create({
	      title: 'Modify your playlist',
	      buttons: [
	        {
	          text: 'Play',
	          handler: () => {
	            console.log('Archive clicked');
	          }
	        },{
	          text: 'Edit',
	          role: 'destructive',
	          handler: () => {
	            this.showPromptForEditPlaylist(this.SelectedID);
	          }
	        },{
	          text: 'Delete',
	          handler: () => {
	            this.deletePlaylist(this.SelectedID);
	          }
	        },{
	          text: 'Cancel',
	          role: 'cancel',
	          handler: () => {
	            console.log('Cancel clicked');
	          }
	        }
	      ]
	    });
	    actionSheet.present();
	  }
}
