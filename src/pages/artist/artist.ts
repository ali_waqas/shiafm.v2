import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { System } from '../../providers/system';
import { Ampache } from '../../providers/ampache';
import { Album } from '../album/album';
import { AlertController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular';
/**
 * Generated class for the Album page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'artist',
  templateUrl: 'artist.html',
})
export class Artist {
	public Artist: object;
	public Album: any;
	constructor(
					public navCtrl: NavController, 
					public navParams: NavParams,
					public SystemService: System,
					public Ampache: Ampache,
					public AlertController: AlertController,
					public ActionSheetController: ActionSheetController,
			   ) {
		this.Artist = this.navParams.get('Artist');
	}

	openAlbum(AlbumID: number){
		this.Ampache.callAPIMethod('getAlbum','&id='+AlbumID)
		.subscribe(
                    data => {
                    	this.Album = this.Ampache.getAmpacheObject(data,'album');
                    	this.navCtrl.push(Album,{Album:this.Album});
                    },
                    error =>{
                    	console.log(error);
                    }
				 );	
	}
}
