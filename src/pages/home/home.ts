import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import { System } from '../../providers/system';
import { Ampache } from '../../providers/ampache';
import { Album } from '../album/album';
import { Artist } from '../artist/artist';
import { Artists } from '../artists/artists';
import { MusicQueue } from '../../providers/music-queue';

import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
	public posts: any;
	public pages: any;
	public Genres: any;
	public Albums: object;
	public Album: object;
	public Artist: any;
	constructor(
				public navCtrl: NavController, 
				public http: Http,
				public SystemService: System,
				public Ampache: Ampache,
				public MusicQueue: MusicQueue
			   ) {
		this.loadAlbums();
	}

	listGenres(){
		this.Ampache.callAPIMethod('getGenres')
		.subscribe(
                    data => {
                    	let Response = this.Ampache.getAmpacheObject(data,'genres');
                    	this.Genres = Response.genre;
                    },
                    error =>{
                    	console.log(error);
                    }
				 );
	}

	loadAlbums(){
	}
}