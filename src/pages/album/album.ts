import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular';
/**
 * Generated class for the Album page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'album',
  templateUrl: 'album.html',
})
export class Album {
	public Album: object;
	public SelectedID: any;

	constructor(
					public navCtrl: NavController, 
					public navParams: NavParams,
					public AlertController: AlertController,
					public ActionSheetController: ActionSheetController,
			   ) {
		this.Album = this.navParams.get('Album');
	}

	ionViewDidLoad() {
	
	}

	presentActionSheet(playlistID: number) {
		this.SelectedID = playlistID;
	    let actionSheet = this.ActionSheetController.create({
	      title: 'Actions',
	      buttons: [
	        {
	          text: 'Play',
	          handler: () => {
	            console.log('Archive clicked');
	          }
	        },{
	          text: 'Like',
	          role: 'destructive',
	          handler: () => {
	            
	          }
	        },{
	          text: 'Share',
	          handler: () => {
	            
	          }
	        },{
	          text: 'Cancel',
	          role: 'cancel',
	          handler: () => {
	            console.log('Cancel clicked');
	          }
	        }
	      ]
	    });
	    actionSheet.present();
	  }

}
