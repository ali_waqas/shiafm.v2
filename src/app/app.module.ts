import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Geners } from '../pages/geners/geners';
import { Album } from '../pages/album/album';
import { Artist } from '../pages/artist/artist';
import { Playlists } from '../pages/playlists/playlists';
import { Artists } from '../pages/artists/artists';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { System } from '../providers/system';
import { Ampache } from '../providers/ampache';
import { MusicQueue } from '../providers/music-queue';

export const firebaseConfig = {
                                apiKey: "AIzaSyCZRL1NxqX7q4bMCKd3NjYD8h9GhxmMyNI",
                                authDomain: "iplus-df171.firebaseapp.com",
                                databaseURL: "https://iplus-df171.firebaseio.com",
                                projectId: "iplus-df171",
                                storageBucket: "iplus-df171.appspot.com",
                                messagingSenderId: "374354219468"
                              };

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Album,
    Artists,
    Artist,
    Geners,
    Playlists
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule, ReactiveFormsModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Album,
    Artist,
    Geners,
    Artists,
    Playlists
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireAuth,
    System,
    Ampache,
    MusicQueue,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
